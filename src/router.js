import Vue from "vue";
import Router from "vue-router";
import Home from "./components/CustomerHome/Home.vue";

Vue.use(Router);

export default new Router({
  mode: "history",
  base: process.env.BASE_URL,
  routes: [{
      path: "/",
      name: "home",
      component: Home
    },
    {
      path: "/restaurants",
      name: "restaurant",

      component: () =>
        import ("./components/CustomerHome/Restaurants.vue")
    },
    {
      path: "/menu/:vendorId",
      name: "menu",

      component: () =>
        import ("./components/CustomerHome/Menu.vue")
    },
    {
      path: "/about",
      name: "about",

      component: () =>
        import ("./views/About.vue")
    },
    {
      path: "/login",
      name: "login",
      component: () =>
        import ("./views/LogIn.vue")
    },
    {
      path: "/vendorreg",
      name: "vendorreg",

      component: () =>
        import (
          "./components/VendorRegistration.vue"
        )
    },

    {
      path: "/loginvendor",
      name: "loginvendor",
      component: () =>
        import ("./components/LoginVendor.vue")
    },

    {
      path: "/forgotpassword",
      name: "forgotpassword",
      component: () =>
        import ("./views/ForgotPassword.vue")
    },
   
    {
      path: "/restroitemsdetail",
      name: "restroitemsdetail",
      component: () =>
        import ("./views/RestroItemsDetail.vue")
    },
    {
      path: "/restro",
      name: "toprestro",
      component: () =>
        import ("./components/UserSearch/TopRestro.vue")
    },
    {
      path: "/menubyrestro",
      name: "toprestro",
      component: () =>
        import ("./components/UserSearch/MenubyVendor.vue")
    },
    {
      path: '/superadmin/',
      name: "superadmin",
      component: () =>
        import ("./components/SuperAdmin/Home.vue"),
        children: [
          {
            path: 'dashboard',
            name: 'adminvendors',
            component: () =>
              import ("./components/SuperAdmin/Dashboard.vue")
          },
          {
          path: 'vendors',
          name: 'adminvendors',
          component: () =>
            import ("./components/SuperAdmin/VendorStatus.vue")
        },
        {
          path: 'settings',
          name: 'adminsettings',
          component: () =>
            import ("./components/SuperAdmin/Settings.vue")
        },
        {
          path: 'adminreg',
          name: 'adminreg',
          component: () =>
            import ("./components/SuperAdmin/AdminRegistration.vue")
        },
        {
          path: 'ordersreport',
          name: 'ordersreport',
          component: () =>
            import ("./components/SuperAdmin/OrderReport.vue")
        },
        {
          path: 'master',
          name: 'master',
          component: () =>
            import ("./components/SuperAdmin/Masters.vue")
        }
      ]
    },
    {
      path: "/vendor/",
      name: "vendorhome",
      component: () =>
        import ("./components/VendorHome/Home.vue"),
        children: [{
          path: 'home',
          name: 'home',
          component: () =>
            import ("./components/VendorHome/OrderDashboard.vue")
        },
        {
          path: "settings",
          name: "settings",
          component: () =>
            import ("./components/VendorHome/Settings.vue")
        },
        {
          path: 'inventory',
          name: "inventory",
          component: () =>
            import ("./components/VendorHome/Inventory.vue")
        },
        {
          path: 'reports',
          name: "reports",
          component: () =>
            import ("./components/VendorHome/Reports.vue")
        }, 
        {
          path: 'orders',
          name: "orders",
          component: () =>
            import ("./components/VendorHome/Orders.vue")
        },
        {
          path: 'createorder',
          name: "createorder",
          component: () =>
            import ("./components/VendorHome/CreateOrder.vue")
        },

        {
          path: 'outlets/edit/:outletId',
          name: "outlets",
          component: () =>
            import ("./components/VendorHome/EditOutlet.vue")
        },
        {
          path: 'outlets',
          name: "outlets",
          component: () =>
            import ("./components/VendorHome/Outlets.vue")
        }

      ]
    },
    

  ]

});
