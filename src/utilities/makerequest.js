import Axios from 'axios';
import getcookie from './getcookie';
import {baseUrl} from '../constants/urlcontants'
export default {
    postAuth(url,data) {
        return new Promise((resolve, reject) => {
             Axios(
                {
                    method: 'post',
                    url: `${baseUrl}${url}`,
                    data: JSON.stringify(data),
                   
                    headers: {
                        'content-type': 'application/json',
                        'Authorization': `${getcookie('token')}`,
                        'accept-language': 'en-US,en;q=0.9'
                    }
                }
            )
                .then((response) => {
                    resolve(response)
                })
                .catch((error) => {
                    reject(error)
                })
        }
        )
    }
    ,


    getAuth(url) {
        return new Promise((resolve, reject) => {
             Axios(
                {
                    method: 'get',
                    url: `${baseUrl}${url}`,
                   
                    headers: {
                        'content-type': 'application/json',
                        'Authorization': `${getcookie('token')}`,
                        'accept-language': 'en-US,en;q=0.9'
                    }
                }
            )
                .then((response) => {
                    resolve(response)
                })
                .catch((error) => {
                    reject(error)
                })
        }
        )
    },
    uploadAuth(url,data){
        return new Promise((resolve,reject)=>{
            Axios(
                {
                    method: 'post',
                    url: `${baseUrl}${url}`,
                    data,
                   
                    headers: {
                        'Content-Type': 'multipart/form-data',
                        'Authorization': `${getcookie('token')}`,
                        'accept-language': 'en-US,en;q=0.9'
                    }
                }
            )
                .then((response) => {
                    resolve(response)
                })
                .catch((error) => {
                    reject(error)
                })
        })
    },
    post(url,data) {
        return new Promise((resolve, reject) => {
             Axios(
                {
                    method: 'post',
                    url: `${baseUrl}${url}`,
                    data: JSON.stringify(data),
                   
                    responseType: 'json',
                    headers: {
                        'content-type': 'application/json'
                    }
                }
            )
                .then((response) => {
                    resolve(response)
                })
                .catch((error) => {
                    reject(error)
                })
        }
        )
    },
    get(url) {
        return new Promise((resolve, reject) => {
             Axios(
                {
                    method: 'get',
                    url: `${baseUrl}${url}`,
                   
                    headers: {
                        'content-type': 'application/json',
                        'accept-language': 'en-US,en;q=0.9'
                    }
                }
            )
                .then((response) => {
                    resolve(response)
                })
                .catch((error) => {
                    reject(error)
                })
        }
        )
    }
    

}