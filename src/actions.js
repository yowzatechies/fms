
import axios from 'axios'
import makerequest from './utilities/makerequest'
import {baseUrl} from './constants/urlcontants'
import getCookie from './utilities/getcookie'
import jwtDecode from 'jwt-decode'

export default 
{
    getVendorsAction({commit,state} , vendors){
      
        makerequest.getAuth(`vendor/all`).then((response)=>{
          
          if(!response.data.error){
            commit('setVendors',response.data.data)
          }
          
        })
    },
    getItemByVendorAction({commit,state} , force){
      var vendorinfo=jwtDecode(getCookie('token'))
      var data={"data":{"vendorId":vendorinfo._id}}
      if(!state.itembyvendor.length || force ){
        makerequest.postAuth(`items/getItemsbyvendor`,data).then((response)=>{
          console.log(response)
          if(response.data.message="success"){
            commit('setItemByVendors',response.data.data)
          }
          
        },
      )
      }
  },
  getVendorDetails({commit,state} , vendors){
    makerequest.getAuth(`vendor/details`).then((response)=>{
      
      if(!response.data.error){
        commit('setVendorInfo',response.data.data)
      }
      
    })
},
getCuisineAction({commit,state} , cuisine){
      
  makerequest.getAuth(`masters/Cuisine/getallcuisine`).then((response)=>{
    
    if(!response.data.error){
      commit('setCuisines',response.data.data)
    }
    
  })
},
 
   
    
  }