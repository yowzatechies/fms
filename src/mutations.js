export default {
  setVendors(state, vendors) {
    // mutate state
    state.vendors = vendors
  },
  setVendorInfo(state, data) {
    state.vendorInfo = data
  },
  
  setItemByVendors(state, data) {
    state.itembyvendor = data
  },
  setCuisines(state, cuisines) {
    state.cuisines = cuisines
  },
  setTrainNo(state,trainNo){
    state.trainNo = trainNo
  }

}