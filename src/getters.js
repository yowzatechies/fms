export default {
  getVendors: (state) => {
    // do any operation here like sorting filtering etc.

    return state.vendors
  },
  getItembyVendor(state){
    return state.itembyvendor
      },
  getVendorInfo: (state) => {
    return state.vendorInfo
  },
  getCuisines: (state) => {
    return state.cuisines
  }

}